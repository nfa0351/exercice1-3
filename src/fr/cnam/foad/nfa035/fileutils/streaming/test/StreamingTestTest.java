package fr.cnam.foad.nfa035.fileutils.streaming.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StreamingTestTest {

    /**
     * teste si les 2 Strings sont null ou non
     */
    @Test
    void difference() {
        String str1 = "toto";
        String str2 = null;
        assertEquals("toto", str1);
        assertEquals(null, str2);
    }

    /**
     * teste si les 2 CharSequence sont égaux ou non
     */
    @Test
    void indexOfDifference() {
        CharSequence cs1 = "abcdef";
        CharSequence cs2 = "abcdeg";
        assertEquals(cs1, cs2);
    }
}
