### Bienvenue dans ce cours CNAM en FOAD, NF035

Vous trouverez dans ce fichier **Readme** l'énoncé de chaque exercice noté dans le cadre du projet tutoré central à cet enseignement sur **JAVA: Bibliothèques et Patterns**.
_La solution de chaque exercice fera d'ailleurs l'objet d'un nouveau projet Git ici-même._

**Même si votre solution est bonne et recevable avec tous les points, il est par ailleurs préférable de repartir de la présente solution pour avancer dans ce projet tutoré sans risque, étant donné ce qui vous sera demandé en suite.**

Voici donc l'énoncé du 3ème exercice noté de la toute première session de ce projet.

---

# Lecture / Ecriture par flux, de fichiers texte ou binaire

## Contexte
* Au programme de ce cours: Exploitation de librairies avancées voir framework
## Objectif
* Mise en application: Utilisation de Log4j pour les messages en sortie console, et de JUnit pour l'industrialisation des tests unitaires.

## Consignes

Exploitation de librairies avancées voir framework: log4j / JunitDans une approche **TDD**, prenez connaissance de ce test écrit en pur Java. Sans en modifier le contenu(*), il va s'agir de développer ce qui est nécessaire à sa compilation ainsi qu'à son exécution.

### Log4J

Se baser sur le site https://mvnrepository.com/ et sur cette configuration...
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="\n********** - Log émit: à %d{HH:mm:ss.SSS}, de niveau %-5level, par %logger{32} - **********************\n\n%msg%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

...pour:

 - [ ] Intégrer au projet la ou les librairies jar nécessaire(s) à l'utilisation de la denière version finale et stable connue de log4j, dans un répertoire lib (Astuce: ne prendre que le strict minimum pour éviter les conflits/surcharges, 2 jars suffisent)
```
J'ai placé dans lib les 2 jars suivants :
log4j-api-2.19.0.jar
log4j-core-2.19.0.jar
```
 - [ ] Positionner la configuration ci-dessus dans le chemin de compilation (src) afin qu'il soit intégré dans le classpath (chemin d'exécution), sous le nom **log4j2.xml** (ceci est une aide supplémentaire, néanmoins il faut être capable de la comprendre à partir des documentations)
```
J'ai placé le fichier de configuration à la racine de src
```
 - [ ] Adapter le script de test pour que cet/ces librairie(s) fasse partie du "chemin de classe "de java"
```
J'ai rajouté "import org.apache.logging.log4j.*;" dans StreamingTest.java
```
 - [ ] Suite à une étude rapide du framework log4j, transformer l'ensemble des sorties console (*System.out.println*) en appels log4j
```
//System.out.println("Cette sérialisation est bien réversible :)");
            Log.info("Cette sérialisation est bien réversible :)");

  //System.out.println("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");
            Log.info("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");
```
 - [ ] Modifier le format de sortie pour que chaque message log tienne sur une ligne et ressemble à:
```
2014-07-02 20:52:39 DEBUG HelloExample:19 - This is debug 
2014-07-02 20:52:39 INFO  HelloExample:23 - This is info 
2014-07-02 20:52:39 WARN  HelloExample:26 - This is warn 
```
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss} %-5level %logger{32}:%L - %msg%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="trace"> <!--level="trace" pour tout logger-->
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```
![échantillon][log4j01]

[log4j01]: log4j01.PNG "log4j01"
![échantillon][log4j02]

[log4j02]: log4j02.PNG "log4j02"


### Junit

 - [ ] Intégrer au projet la ou les librairies jar nécessaire(s) à l'utilisation de la denière version finale et stable connue de JUnit, dans le répertoire lib
```
J'ai placé dans lib les jars suivants:
apiguardian-api-1.1.2.jar
junit-jupiter-5.9.1.jar
junit-jupiter-api-5.9.1.jar
junit-jupiter-engine-5.9.1.jar
junit-jupiter-params-5.9.1.jar
junit-platform-commons-1.9.1.jar
junit-platform-engine-1.9.1.jar
opentest4j-1.2.0.jar
```
 - [ ] Porter le plus simplement possible le code d'une de nos 2 classes de test existantes sous la forme de tests unitaires JUnit. 
```java
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StreamingTestTest {

    /**
     * teste si les 2 Strings sont null ou non
     */
    @Test
    void difference() {
        String str1 = "toto";
        String str2 = null;
        assertEquals("toto", str1);
        assertEquals(null, str2);
    }

    /**
     * teste si les 2 CharSequence sont égaux ou non
     */
    @Test
    void indexOfDifference() {
        CharSequence cs1 = "abcdef";
        CharSequence cs2 = "abcdeg";
        assertEquals(cs1, cs2);
    }
}
```
 - [ ] Apporter une preuve en image (screenshot laissant apparaitre à la fois la progression Junit et la sortie console)
   ![échantillon][junit01]

[junit01]: junit01.PNG "junit01"
